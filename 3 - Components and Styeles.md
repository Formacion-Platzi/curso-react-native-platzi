
# *************************************************************************************************** #
# *************************************  Components and Styles  ************************************* #
# *************************************************************************************************** #

# Componentes de react-native

- Estilos en react native

    Si quisieramos dar estilos a nuestros componentes podiamos hacerlo dentro de los mismo o... 
    podriamos usar StyleSheet que es una herramienta de react native para agregar estilos por suera de 
    los componentes.

    Ej.: const <name_> = StyleSheet.create({ ... })

# Componentes basicos:

    - Text: Elemento que nos sirve para colocar texto.
    - View : Es como un apartado para poner elementos. Es similar a un <div>  de HTML.
    - Image: Para insertar unaimagen
    - TextInput: Es un input tipico.
    - ScrollView: Para tener elementos que se puedan scrollear
    - StyleSheet: Para añadir estilos a los elementos.

    Obs.:
        Titulo: Lorem Picsum
        Descripcion: Para tener una imagen random podemos pedirsela a esta pagina que no lo retorna 
        con el tamaño que le pidamos.
        Page: https://picsum.photos/


# Otros componentes

    - Button: Nos lo provee react-native. Es un componente que nos permite un minimo nivel de 
      personalizacion.
    - TouchableOpacity: Si nosotros quisieramos un button mas personalizable, usariamos este 
      componente.

- Herramientas utiles para multiples sistemas op. moviles (Android & iOS): Con esto podemos padar dos
  valores diferentes para cada uno de los sistemas op. Ej. podriamos definir un color de letra para 
  andriod y otro color de letra para ios.

    - Platform.select({ 
        ios: '...',
        android: '...'
     });

# Problemas con el header en iOS

Esto es un problema que pasa en el iPhono X gracias al notch. Para ello hay una solucion que implementaron 
los desarrolladores de React-Native, que es <SafeAreaView/>. Es un componente de area segura. Se lo 
encapsula al header como si fuera un view mas.

# Composicion de componetes

¿ Como hacemos para pasar elementos (Text, Button, ...) desde un componente a  otro componente ?

Ej.: Home -> Header. En el cual Home le pasa 3 <Text/>, y Header lo recibe con { props.children }

    class Home extends React.Component 
    {
        render(){
            return (
                <Header>
                    <Text>Text 1</Text>
                    <Text>Text 2</Text>
                    <Text>Text 2</Text>
                </Header>
            )
        }
    }

    function Header(props) {
        return(
            <View>
                { props.children }
            </View>
        )
    }

# Componentes para crear listas (FlatList & SectionList)

Si queremos hacer una lista muy sencilla, flatList sirve. Pero si se quiere hacer una lista anidada-
compleja, en este caso SectionList es la opcion.

# Para subir archivos y obtener una url (sin registrarse)
    https://anonymousfiles.io/

# *************************************************************************************************** #