
# Emulacion

Tenemos dos formas de hacerlo, sin muchas instalaciones innecesarias. Local y Remota.

* Local
    1. Ejecutar:
        npm start
    2. Observar que tenemos de correr la opcion web. Presionar 'w'.
        To run the app, choose one of:
        › Scan the QR code above with the Expo app (Android) or the Camera app (iOS).
        › Press a for Android emulator, or w to run on web.
        › Press e to send a link to your phone with email.
    3. Nos abrira el browser y veremos corriendo nuestra app.

* Remota
    1. Necesitamos la app 'Expo GO' para el celular.
        Ir a la tienda Play Store.
    2. Permitir depuracion USB en el celular.
        Ir a Ajustes > Modem USB/... > Modem USB y habilitarlo.
    3. Conectar a traves del USB la Notebook y el celular.
    2. Corremos nuestra app con 'npm start'.
    3. Veremos que nos genera un codigo QR. Luego a traves de la app Expo GO, debemos escanear el codigo.
    4. Veremos corriendo nuestra app corriendo sobre nuestro celular.
   
    * IMPORTANTE: Debe en todo momento estar conectado por USB. Ya que es el medio por el cual translada nuestra app.

* Para inpeccionar desde el browser.
    Agregar la extension 'React-DevTools' si lo corre on browser.
        - https://addons.mozilla.org/es/firefox/addon/react-devtools/

# Inspeccionar

Tenemos dos formas de hacer la inspeccion de nuestra aplicacion: Local y Remota.

* Local
    1. Ingrese a la app con el codigo QR.
    2. Presione continuamente el boton de menu del celular (Izq.).
    3. Aparecera ditintas opciones entre la que encontrara 'Show Element Inspector'.
    4. Seleccione el elemento y vera los detalles del componente en el celular.

* Remoto
    1. Debera instalar react-devtools
        npm install -g react-devtools
    2. Ejecutarlo con 
        react-devtools
    3. Observara que espera a que se conecte un dispositivo.
        Luego hacer 1. 2. 3. 4 de 'Local'.
    4. Notara que se conecta al dispositivo y al seleccionar cualquier elemento desde el celular
       este pasara a verse seleccionado en la ventana de react-devtools.

# Como llevar a produccion nuestro proyecto con expo (generar un instalador apk)

Title: React Native, Curso para Principiantes (usando Expo)
Ref.: https://www.youtube.com/watch?v=hXDMWeD0ERM

1º
    Ejecutamos => expo build:android
2º
    Le damos un nombre a nuestro package, por lo general se llaman del tipo 'com.the1984.<name_project>'
3º 
    Nos logueamos con nuestra cuenta expo. Si no lo tenemos, nos cramos una en => https://expo.io/signup
4º
    Elegimos apk
5º
    Esperamos a que quede en "- Build queued", eso significa que se termino de subir a la nube nuestro proyecto
    luego hay que esperar a nuestro turno para que genere el apk.
