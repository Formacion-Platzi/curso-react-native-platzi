
# Creacion del proyecto con expo 
* Instacion de herramienta expo para 
    npm install -g expo-cli
* Creacion del proyecto
    expo init <name_app>

# Creacion del proyecto con create-react-native-app
* Instalacion de la libreria que permite crear mucho mas facil y rapido un entorno react app movile.
    npm install -g create-react-native-app
* Creacion del proyecto
    create-react-native-app <name_app>
